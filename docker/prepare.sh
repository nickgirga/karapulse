set -eux;

apt-get update

apt-get install -y --no-install-recommends \
    python3-pip \
    python3-setuptools \
    ninja-build \
    git \
    curl \
    gettext \
    appstream-util \
    libsqlite3-dev \
    pkg-config \
    libglib2.0-dev \
    libgtk-3-dev \
    libssl-dev \
    desktop-file-utils \
    libgstreamer-plugins-base1.0-dev \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-libav \
    gstreamer1.0-tools \
    gstreamer1.0-gtk3 \
    gstreamer1.0-gl \
    gstreamer1.0-x \
    python3-dev

# Install Rust
RUSTUP_VERSION=1.21.1
RUST_VERSION="nightly-2020-04-10"
RUST_ARCH="x86_64-unknown-linux-gnu"

apt install -y wget
RUSTUP_URL=https://static.rust-lang.org/rustup/archive/$RUSTUP_VERSION/$RUST_ARCH/rustup-init
wget $RUSTUP_URL
apt remove -y wget

chmod +x rustup-init;
./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION;
rm rustup-init;
chmod -R a+w $RUSTUP_HOME $CARGO_HOME

rustup component add clippy-preview
cargo install --force cargo-tarpaulin
cargo install --force cargo-audit
cargo install --force --git https://github.com/kbknapp/cargo-outdated

rustup --version
cargo --version
rustc --version

pip3 install meson==0.54.3

curl -sL https://deb.nodesource.com/setup_12.x | bash -

apt-get install -y --no-install-recommends \
    nodejs \

# spleeter, needed for Karamel
pip3 install spleeter==1.4.9
