import {Component, Input, OnInit} from '@angular/core';
import {Song} from '../song';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material';
import {SongDetailsComponent} from '../song-details/song-details.component';


@Component({
  selector: 'app-searchresults',
  templateUrl: './searchresults.component.html',
  styleUrls: ['./searchresults.component.css']
})
export class SearchResultsComponent implements OnInit {
  @Input() results: Song[];
  displayedColumns: string[] = ['artist', 'title'];
  constructor(private bottomSheet: MatBottomSheet) { }

  ngOnInit() {
  }

  openBottomSheet(row): void {
    console.log(row);
    this.bottomSheet.open(SongDetailsComponent, {data: row});
  }

}
