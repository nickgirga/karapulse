// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use std::path::PathBuf;

pub enum TestMedia {
    #[allow(dead_code)]
    Video2s,
    #[allow(dead_code)]
    Video2_2s,
    #[allow(dead_code)]
    VideoTagged,
    #[allow(dead_code)]
    Audio,
    #[allow(dead_code)]
    Invalid,
}

pub fn get_media_test_dir() -> PathBuf {
    let mut path = PathBuf::new();
    path.push("tests");
    path.push("medias");

    path
}

impl TestMedia {
    pub fn path(&self) -> PathBuf {
        let mut path = get_media_test_dir();
        path.push(match self {
            TestMedia::Video2s => "video-2s.mkv",
            TestMedia::Video2_2s => "video2-2s.mkv",
            TestMedia::VideoTagged => "video-tagged.mkv",
            TestMedia::Audio => "test.mp3",
            TestMedia::Invalid => "DoesNotExist",
        });

        path
    }
}
