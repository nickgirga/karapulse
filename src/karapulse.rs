// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use failure::Error;
use gdk::prelude::*;
use glib::{Receiver, Sender};
use gtk::prelude::*;
use serde::Serialize;
use std::cell::{Cell, RefCell};
use std::convert::TryInto;
use std::ops;
use std::path::{Path, PathBuf};
use std::rc::Rc;
use std::sync::mpsc;

use crate::db::{Song, DB};
use crate::player;
use crate::protocol;
use crate::queue::{Queue, QueueItem};

// The fallback song duration, in seconds, used to compute queue ETA
const FALLBACK_ETA_DURATION: u32 = 3 * 60;

#[allow(dead_code)]
#[derive(Debug, PartialEq, Clone, Copy, Serialize)]
pub enum State {
    Waiting,
    Playing,
    #[allow(dead_code)]
    Paused,
}

#[derive(Debug, Clone)]
pub enum Message {
    Play,
    Pause,
    Next,
    PlayingDone,
    Enqueue {
        path: String,
        user: String,
        song: Song,
    },
    GetStatus {
        reply_tx: mpsc::Sender<Reply>,
    },
}

impl PartialEq for Message {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Message::Play, Message::Play) => true,
            (Message::Pause, Message::Pause) => true,
            (Message::Next, Message::Next) => true,
            (Message::PlayingDone, Message::PlayingDone) => true,
            // reply_tx don't have to be equals
            (Message::GetStatus { .. }, Message::GetStatus { .. }) => true,
            (
                Message::Enqueue {
                    path: path_a,
                    user: user_a,
                    song: song_a,
                },
                Message::Enqueue {
                    path: path_b,
                    user: user_b,
                    song: song_b,
                },
            ) => path_a == path_b && user_a == user_b && song_a == song_b,
            _ => false,
        }
    }
}

#[derive(Debug)]
pub enum Reply {
    Status { status: protocol::StatusResponse },
}

pub struct KarapulseInner {
    player: player::Player,
    queue: RefCell<Queue>,
    state: Cell<State>,
    current_song: RefCell<Option<QueueItem>>,
    db: DB,
}

pub struct Karapulse {
    inner: Rc<KarapulseInner>,
}

impl KarapulseInner {
    pub fn new(player: player::Player, db: DB) -> Self {
        Self {
            player,
            queue: RefCell::new(Queue::new()),
            state: Cell::new(State::Paused),
            current_song: RefCell::new(None),
            db,
        }
    }

    pub fn enqueue(
        &self,
        path: &PathBuf,
        user: &str,
        song: Option<&Song>,
        history_id: Option<i64>,
    ) -> Result<(), Error> {
        let history_id = if let Some(id) = history_id {
            Some(id)
        } else if let Some(song) = song {
            Some(self.db.add_history(&song, &user)?)
        } else {
            None
        };

        self.queue.borrow_mut().add(path, user, song, history_id);

        if self.state.get() == State::Waiting {
            self.play_next()?;
        }

        Ok(())
    }

    fn set_state(&self, state: State) {
        let current_state = self.state.get();
        if current_state != state {
            debug!(
                "set state: {:?} -> {:?} (player: {:?})",
                current_state,
                state,
                self.player.get_state().unwrap()
            );
            self.state.set(state);
        }
    }

    fn play_next(&self) -> Result<(), Error> {
        loop {
            match self.queue.borrow_mut().next() {
                Some(item) => {
                    if let Some(history_id) = item.history_id {
                        self.db.set_history_played(history_id)?;
                    }

                    debug!("next: {}", item.path.display());
                    if self.player.open_media(&item.path).is_ok() {
                        self.set_state(State::Playing);
                        self.current_song.replace(Some(item));
                        break;
                    }
                }
                None => {
                    if self.state.get() != State::Waiting {
                        self.player.stop()?;
                        debug!("waiting for songs");
                        self.set_state(State::Waiting);
                        self.current_song.replace(None);
                    }
                    break;
                }
            }
        }

        Ok(())
    }

    pub fn play(&self) -> Result<(), Error> {
        match self.state.get() {
            State::Paused => match self.player.get_state()? {
                player::State::Waiting => self.play_next()?,
                player::State::Paused => {
                    self.set_state(State::Playing);
                    self.player.play()?;
                }
                _ => unreachable!(),
            },
            State::Waiting => self.play_next()?,
            State::Playing => {}
        }

        Ok(())
    }

    fn pause(&self) -> Result<(), Error> {
        match self.state.get() {
            State::Playing => self.player.pause()?,
            State::Paused | State::Waiting => {}
        }

        self.set_state(State::Paused);
        Ok(())
    }

    fn toggle_pause(&self) -> Result<(), Error> {
        match self.state.get() {
            State::Playing => self.pause(),
            State::Paused => self.play(),
            State::Waiting => Ok(()),
        }
    }

    #[allow(dead_code)]
    pub fn state(&self) -> State {
        self.state.get()
    }

    pub fn get_status(&self, reply_tx: &mpsc::Sender<Reply>) -> Result<(), Error> {
        let state = self.state();
        let current_song = self.current_song.borrow().clone();
        let position = match state {
            State::Playing => self.player.get_position(),
            _ => None,
        };
        let queue = self.queue.borrow().snapshot();

        // Compute ETA for each song in the queue
        let duration = current_song
            .as_ref()
            .and_then(|i| i.song.as_ref())
            .and_then(|s| s.length);

        let mut eta = match (position, duration) {
            (Some(position), Some(duration)) => (duration as u32) - (position as u32),
            // Assume song is already half played
            _ => FALLBACK_ETA_DURATION / 2,
        };

        let mut queue_response = Vec::new();
        for i in queue {
            let increment = match i.song {
                Some(ref song) => song
                    .length
                    .unwrap_or_else(|| FALLBACK_ETA_DURATION.try_into().unwrap())
                    as u32,
                _ => FALLBACK_ETA_DURATION,
            };

            queue_response.push(protocol::StatusResponseSong::new(i, Some(eta)));
            eta += increment;
        }

        let current_song = current_song.map(|s| protocol::StatusResponseSong::new(s, None));

        let status = protocol::StatusResponse::new(state, current_song, position, queue_response);
        reply_tx.send(Reply::Status { status })?;
        Ok(())
    }
}

impl ops::Deref for Karapulse {
    type Target = KarapulseInner;

    fn deref(&self) -> &KarapulseInner {
        &*self.inner
    }
}

impl Karapulse {
    pub fn new(window: gtk::Window, tx: Sender<Message>, rx: Receiver<Message>, db: DB) -> Self {
        let player = player::Player::new(window.clone(), tx).unwrap();
        let inner = KarapulseInner::new(player, db);
        let inner = Rc::new(inner);

        let inner_weak = Rc::downgrade(&inner);
        rx.attach(None, move |msg| {
            let karapulse = match inner_weak.upgrade() {
                Some(inner) => inner,
                None => return glib::Continue(false),
            };

            let res = match msg {
                Message::Play => karapulse.play(),
                Message::Pause => karapulse.pause(),
                Message::Next | Message::PlayingDone => karapulse.play_next(),
                Message::Enqueue {
                    ref path,
                    ref user,
                    ref song,
                } => karapulse.enqueue(&Path::new(path).to_path_buf(), &user, Some(song), None),
                Message::GetStatus { ref reply_tx } => karapulse.get_status(reply_tx),
            };

            if let Err(e) = res {
                error!("failed to handle message {:?}: {}", msg, e.to_string())
            }
            glib::Continue(true)
        });

        // Hide cursor in fullscreen
        window.connect_window_state_event(|window, event| {
            let change = event.get_changed_mask();
            if change.contains(gdk::WindowState::FULLSCREEN) {
                let new_state = event.get_new_window_state();
                let win = window.get_window().unwrap();

                let cursor = if new_state.contains(gdk::WindowState::FULLSCREEN) {
                    gdk::Cursor::new(gdk::CursorType::BlankCursor)
                } else {
                    gdk::Cursor::new(gdk::CursorType::Arrow)
                };

                win.set_cursor(Some(&cursor));
            }
            Inhibit(false)
        });

        let inner_weak = Rc::downgrade(&inner);
        window.connect_key_press_event(move |window, key| {
            let karapulse = match inner_weak.upgrade() {
                Some(inner) => inner,
                None => return Inhibit(false),
            };

            let keyval = key.get_keyval();
            match keyval {
                gdk::keys::constants::f => {
                    let win = window.get_window().unwrap();
                    let state = win.get_state();
                    if state.contains(gdk::WindowState::FULLSCREEN) {
                        window.unfullscreen();
                    } else {
                        window.fullscreen();
                    }
                }
                gdk::keys::constants::Escape => window.unfullscreen(),
                gdk::keys::constants::space => karapulse
                    .toggle_pause()
                    .unwrap_or_else(|e| error!("failed to toggle pause: {}", e.to_string())),
                _ => {}
            }
            Inhibit(false)
        });

        window.set_size_request(800, 600);
        window.fullscreen();
        window.show_all();

        Self { inner }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::init;
    use crate::tests::TestMedia;
    use glib::Sender;

    struct Test {
        karapulse: Karapulse,
        tx: Sender<Message>,
    }

    impl Test {
        fn new() -> Test {
            let _ = env_logger::try_init();
            init().unwrap();

            let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
            let window = gtk::Window::new(gtk::WindowType::Toplevel);
            let db = DB::new_memory().unwrap();

            let karapulse = Karapulse::new(window, tx.clone(), rx, db);

            Test { karapulse, tx }
        }

        fn tick(&mut self) {
            while gtk::events_pending() {
                gtk::main_iteration_do(false);
            }
        }

        fn send_msg(&mut self, msg: Message) {
            self.tx.send(msg).unwrap();
            self.tick();
        }

        fn pause(&mut self) {
            self.send_msg(Message::Pause);
        }

        fn play(&mut self) {
            self.send_msg(Message::Play);
        }

        fn next(&mut self) {
            self.send_msg(Message::Next);
        }

        fn wait_while(&mut self, state: State) {
            while self.karapulse.state() == state {
                self.tick();
            }
        }

        fn enqueue(&mut self, media: TestMedia) {
            self.karapulse
                .enqueue(&media.path(), "test", None, None)
                .unwrap();
        }
    }

    #[test]
    #[ignore]
    fn play_pause() {
        let mut test = Test::new();
        assert_eq!(test.karapulse.state(), State::Paused);

        // One song in the initial queue
        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Paused);

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Playing);

        // Pause
        test.pause();
        assert_eq!(test.karapulse.state(), State::Paused);

        // Resume
        test.play();
        assert_eq!(test.karapulse.state(), State::Playing);

        // Wait for the song to finish
        test.wait_while(State::Playing);
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.pause();
        assert_eq!(test.karapulse.state(), State::Paused);
        test.play();
        assert_eq!(test.karapulse.state(), State::Waiting);
    }

    #[test]
    #[ignore]
    fn play_no_song() {
        let mut test = Test::new();

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
    }

    #[test]
    #[ignore]
    fn invalid_media() {
        /* Play, enqueue invalid */
        let mut test = Test::new();

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Invalid);
        assert_eq!(test.karapulse.state(), State::Waiting);

        /* Enqueue invalid, play */
        let mut test = Test::new();
        test.enqueue(TestMedia::Invalid);
        assert_eq!(test.karapulse.state(), State::Paused);

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Waiting);

        /* Enqueue invalid, enqueue valid, play */
        let mut test = Test::new();
        test.enqueue(TestMedia::Invalid);
        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Paused);

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Playing);

        /* Enqueue valid, enqueue invalid, play */
        let mut test = Test::new();
        test.enqueue(TestMedia::Video2s);
        test.enqueue(TestMedia::Invalid);
        assert_eq!(test.karapulse.state(), State::Paused);

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Playing);

        test.wait_while(State::Playing);
        assert_eq!(test.karapulse.state(), State::Waiting);
    }

    #[test]
    #[ignore]
    fn next() {
        let mut test = Test::new();

        assert_eq!(test.karapulse.state(), State::Paused);
        test.next();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.next();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.next();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.enqueue(TestMedia::Video2s);
        test.next();
        assert_eq!(test.karapulse.state(), State::Playing);
        test.next();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.pause();
        assert_eq!(test.karapulse.state(), State::Paused);
        test.next();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.pause();
        assert_eq!(test.karapulse.state(), State::Paused);
        test.next();
        assert_eq!(test.karapulse.state(), State::Playing);
        test.next();
        assert_eq!(test.karapulse.state(), State::Waiting);
    }
}
